# -*- coding: utf-8 -*-
"""
Created on Sun Apr 28 00:01:49 2019

@author: Philipp
"""
import os
import sys
import math
import click
import ntpath
import numpy as np 
import tensorflow as tf

from PIL import Image
#from cv2 import equalizeHist
from keras import metrics, losses
from operator import itemgetter
from keras.models import load_model

FG                  = ".fg"
GT                  = ".gt"
BMP                 = ".bmp" 
DIR                 = ".dirmap"

ROWS_FP             = 564
COLS_FP             = 452
ROWS_FG             = 70
COLS_FG             = 56

NUMBER_OF_CLASSES   = 256
TOP_K               = 256
PIPE_NAME           = "\\\\.\\pipe\\PipeForFVCOrientation"
FILENAME            = "model_201908191950_CV_SGD_0.h5"
FILEHEADER          = "DIRIMG00"

def processInput(indexfile, outputFolder):
    """
    Reads from the index file. Index file looks like this:
        '4
        pathToFile step border
        pathToFile step border
        pathToFile step border
        pathToFile step border'
        
    where the first line contains an int representing the number of lines 
    in this file, step and border are integers, and pathToFile represents 
    the location of the fingerprint file.
    
    Additionally to the fingerprint we also extract the foreground file with the 
    same name but a different extension.
    """
        
    with open(indexfile, "rt") as file:
        imagesCount = int(file.readline()) # first line contains nr of images
        cnn = load_model(resource_path('{}'.format(FILENAME)), 
                         custom_objects={'loss':loss, 'accuracy': accuracy})

        namedPipe = open(PIPE_NAME, "wt")
        
        for i in range(0, imagesCount):
            line = file.readline()
            # each line holds: filename, step length, indicator for top left corner
            path, step, border = line.split(' ') 
            # remove file extension from path
            path = ntpath.splitext(path)[0]
            # get file name
            name = os.path.basename(path)
            # create path for dirmap file
            outputFilePath = outputFolder+"\\"+name+".dirmap"
            # read foreground file
            fgOrig = getFGOrig(path+FG)
            # read fingerprint image
            fp = getFingerprint(path+BMP, fgOrig, int(border), int(step))
            testModel(fp, fgOrig, path, step, border, outputFilePath, cnn, namedPipe)
        
    namedPipe.close()


def getForground(filePath):
    with open(filePath, "rt") as bitmap:
        rowString, _ = bitmap.readline().split()
        rows = int(rowString)
                    
        fg = [[0 for i in range(COLS_FG)] for j in range(ROWS_FG)]
        for i in range(rows):
            line = bitmap.readline()
            j = 0
            for string in line.split():
                 fg[i][j] = int(string)
                 j = j+1
    return np.asarray(fg)


def getFGOrig(filePath):
    with open(filePath, "rt") as bitmap:
        (rowString, colString) = bitmap.readline().split()
        rows = int(rowString)
        cols = int(colString)
        fg = [[0 for i in range(cols)] for j in range(rows)]
                    
        for i in range(rows):
            line = bitmap.readline()
            j = 0
            for string in line.split():
                fg[i][j] = int(string)
                j = j+1
    return np.array(fg)

    

def getFingerprint(filePath, fg, border, step):
    """
    Returns the fingerprint stored in 'filePath' after (applying
    histogram equalization) and embedding the image in a uniform canvas.
    """     
    data = np.array(Image.open(filePath))
    #data = histEqualization(data)
    data = normalize(data, fg, border, step)
    black = np.min(data)
    canvas = [[black for i in range(COLS_FP)] for j in range(ROWS_FP)]
    for i in range(0, data.shape[0]):         # fill canvas with data
        for j in range(0, data.shape[1]):
            canvas[i][j] = data[i][j]
    return np.asarray(canvas)


def normalize(fp, fg, border, step):
    fgList = fg.flatten().tolist()
    ctr = 0
    fg_values = [0]*fgList.count(1)
    for row in range(border, fp.shape[0]-border, step):
        for col in range(border, fp.shape[1]-border, step):
            if(fgList[ctr] == 1):
                for i in range(0, step, 1):
                    for j in range(0, step, 1):
                        fg_values.append(fp[row+i][col+j])
            else:
                for i in range(0, step, 1):
                    for j in range(0, step, 1):
                        fp[row+i][col+j] = 0
            ctr += 1
    
    #remove remaining background (bottom) (if existing)
    for i in range(fp.shape[0]-border, fp.shape[0]):
        for j in range(0, fp.shape[1]):
            fp[i][j] = 0
    #remove remaining background (right) (if existing)
    for i in range(0, fp.shape[0]):
        for j in range(fp.shape[1]-border, fp.shape[1]):
            fp[i][j] = 0
    #remove remaining background (top) (if existing)
    for i in range(0, border):
        for j in range(0, fp.shape[1]):
            fp[i][j] = 0
    #remove remaining background (left) (if existing)
    for i in range(0, fp.shape[0]):
        for j in range(0, border):
            fp[i][j] = 0      
            
    #compute mean and std from foreground pixels only 
    m = np.mean(fg_values)
    std = np.std(fg_values)
    
    #apply normalization to the whole image (we don't care about background)
    fp = fp-m
    fp = fp/std
    
    #scale back to range [0, 1]
    fp -= np.min(fp)
    fp /= np.max(fp)
    return fp


def minMaxNorm(X):
    X = np.asarray(X)
    return X/255


def accuracy(y_true, y_pred): 
    zero = tf.constant(0, 'int32')
    zero_oh = tf.one_hot(zero, 256)
    mask = 1-zero_oh
    y_pred_masked = y_pred*mask
    y_true_masked = y_true*mask
    return metrics.categorical_accuracy(y_true_masked, y_pred_masked)


def loss(y_true, y_pred):
    zero = tf.constant(0, 'int32')
    zero_oh = tf.one_hot(zero, 256)
    mask = 1-zero_oh
    y_pred_masked = y_pred*mask
    y_true_masked = y_true*mask
    return losses.categorical_crossentropy(y_true_masked, y_pred_masked)


def testModel(x, fgOrig, path, step, border, outputPath, cnn, namedPipe):
    x = x.reshape(1, ROWS_FP, COLS_FP, 1)
    predictions = cnn.predict(x)
    idx = np.arange(NUMBER_OF_CLASSES)
    rows = fgOrig.shape[0]
    cols = fgOrig.shape[1]

    f=open(outputPath,'wb')
    try:
        f.write(FILEHEADER.encode())
        f.write(int(border).to_bytes(4, byteorder='little'))
        f.write(int(border).to_bytes(4, byteorder='little'))
        f.write(int(step).to_bytes(4, byteorder='little'))
        f.write(int(step).to_bytes(4, byteorder='little'))
        f.write((cols).to_bytes(4, byteorder='little'))
        f.write((rows).to_bytes(4, byteorder='little'))

        for i in range(predictions.shape[0]):    
            for row in range(rows):
                for col in range(cols):
                    if(fgOrig[row][col]):
                        class_pred_lst = list(zip(idx, predictions[i][row][col]))
                        # sort lst by prediction (highest pred first)
                        class_pred_lst = sorted(class_pred_lst, key=itemgetter(1))
                        # take the best TOP_K entries 
                        tupleList = class_pred_lst[-TOP_K:]
                        
                        f.write(int(getTOP_KWeightedOrientation(tupleList)).to_bytes(1, byteorder='little'))
                        f.write((255).to_bytes(1, byteorder='little'))
                    else:
                        f.write((0).to_bytes(1, byteorder='little'))
                        f.write((0).to_bytes(1, byteorder='little')) 
    except:
        print("Cannot open file {}".format(outputPath))
    finally:
        f.close()
        print("Closed file successfully!")       
    
    print("Writing {} to named pipe...".format(path))
    namedPipe.write(path)
    namedPipe.flush()
    print("Wrote to named pipe successfully!")


def getTOP_KWeightedOrientation(tupleLst):
    # save predictions in additional list for scaling
    # neccessary if we don't take all the predictions into account
    # i.e. if TOP_K < 256
    probaList = []
    for t in tupleLst:
        probaList.append(t[1])
        
    # scale probabilities (sum should add up to 1)
    probaList = scaleProba(probaList)
    j = 0
    x = 0
    y = 0
    for t in tupleLst:
        deg = convertToDegree(t[0])
        rad = math.radians(deg)
        sin = math.sin(2*rad)
        cos = math.cos(2*rad)
        # weighted average of scaled probabilities
        x += cos*probaList[j]
        y += sin*probaList[j]
        j+=1
        
    degree = math.degrees(math.atan2(y, x)/2)
    if(degree < 0):
        degree = degree+180
        
    return convertToByteRange(degree)
                
                
def scaleProba(probaList): 
    s = sum(probaList)
    probaList = [p * 1/s for p in probaList] 
    return probaList  


def convertToDegree(x):
    return x*180/256


def convertToByteRange(x):
    """
    Returns a 'byte representation' of an orientation angle (in units of 180/256)
    """
    return int(x*256/180)


def resource_path(relative_path):
    """ 
    Get absolute path to resource, works for dev and for PyInstaller 
    """
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)
  
    
@click.command()
@click.argument('index')
@click.argument('output')
def cli(index, output):
    processInput(index, output)

if __name__ =='__main__':
    cli()


