# -*- coding: utf-8 -*-
"""
Created on Sat Feb 15 14:16:11 2020

@author: Philipp
"""
import keras
import numpy as np

from Cyclical_Callback import CyclicLR
from keras.callbacks import LearningRateScheduler
import LRFinder
import utils.constants as const


def invers_decay_schedule(initial_lr=const.initial_lr, gamma=const.gamma, power=const.power):
    '''
    Wrapper function to create a LearningRateScheduler with invers decay schedule.
    '''
    def schedule(epoch):
        lr = initial_lr * (1 + gamma * epoch)**(-power)
        return lr
    
    return LearningRateScheduler(schedule)


def inverse_decay():
    return invers_decay_schedule()


def early_stopping(patience=4):
    return keras.callbacks.EarlyStopping(monitor='val_loss', 
                                         min_delta=0.0012, 
                                         patience=patience,
                                         verbose=0, 
                                         mode='auto')

def cyclic_LR(initial_lr=const.initial_lr, max_lr=const.max_lr, step_size=const.step_size):
    return CyclicLR(base_lr=initial_lr, 
                   max_lr=max_lr,
                   step_size=step_size,
                   mode='triangular')
 
    
def lr_finder():
    return LRFinder(min_lr=const.initial_lr,
                    max_lr=const.max_lr,
                    steps_per_epoch=np.ceil(const.NUMBER_OF_EPOCHS), 
                    epochs=2)


def reduce_lr_on_plateau():
    return ReduceLROnPlateau(monitor='val_loss', 
                             factor=0.1,
                             patience=2, 
                             min_lr=min_lr)
