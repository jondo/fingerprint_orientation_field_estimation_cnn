# -*- coding: utf-8 -*-
"""
Created on Sat Feb 15 13:59:59 2020

@author: Philipp
"""

#path to the index file
INDEXFILE           = "Data/index.txt"
SEP                 = "/"
FG                  = ".fg"
GT                  = ".gt"
BMP                 = ".bmp" 

SEED                = 135

ROWS_FP             = 576
COLS_FP             = 464
ROWS_FG             = 72
COLS_FG             = 58

NUMBER_OF_CLASSES   = 256
NUMBER_OF_EPOCHS    = 120 #600
NR_OF_IMAGES        = 60
SPLITRATE           = 0.8
TOP_K               = 256
FOLDS               = 5

# conv layer identifier #change after every run...
nr                  = 1

TRAINING            = True
AUGMENTATION        = True
ADD_NOISE           = True 
# TODO flipping left-right and up-down remains to be implemented
FLIP_LR             = False 
FLIP_UD             = False
FLIP_LR_AND_UD      = False
LAPLACE             = False
INVERT              = False
CROSSVAL            = True

# hyperparameter section
batch_size          = 7
initial_lr          = 12*1e-6
max_lr              = 1*1e-5
momentum            = 0.9
alpha               = 0.32#36
shuffle             = True  
decay               = 5*1e-3
gamma               = 1e-4
power               = 0.75
weight_decay        = 3*1e-5
model_path          = 'Models/my_model'

step_size = 1000#40*len(x_train)



