# -*- coding: utf-8 -*-
"""
Created on Sat Feb 15 14:01:33 2020

@author: Philipp
"""
import cv2
import utils.constants as const

import numpy as np

from skimage import exposure


def normalize(fp, fg, border, step):
    fgList = fg.flatten().tolist()
    ctr = 0
    fg_values = [0]*fgList.count(1)
    borderHalf = int(border/2)
    for row in range(border, fp.shape[0]-border, step):
        for col in range(border, fp.shape[1]-border, step):
            if(fgList[ctr] == 1):
                fg_values.append(fp[row][col])
            else:
                for i in range(-borderHalf, borderHalf, 1):
                    for j in range(-borderHalf, borderHalf, 1):
                        fp[row+i][col+j] = 0
            ctr += 1
    
    #remove remaining background (bottom) (if existing)
    for i in range(fp.shape[0]-border, fp.shape[0]):
        for j in range(0, fp.shape[1]):
            fp[i][j] = 0
    #remove remaining background (right) (if existing)
    for i in range(0, fp.shape[0]):
        for j in range(fp.shape[1]-border, fp.shape[1]):
            fp[i][j] = 0
    #remove remaining background (top) (if existing)
    for i in range(0, border):
        for j in range(0, fp.shape[1]):
            fp[i][j] = 0
    #remove remaining background (left) (if existing)
    for i in range(0, fp.shape[0]):
        for j in range(0, border):
            fp[i][j] = 0      
            
    #compute mean and std from foreground pixels only 
    m = np.mean(fg_values)
    std = np.std(fg_values)
    #apply normalization to the whole image (we don't care about background)
    fp = fp-m
    fp = fp/std
    
    fp -= np.min(fp)
    fp /= np.max(fp)
    return fp


def splitTrainData(x, y):
    splitIdx = int(const.NR_OF_IMAGES*0.8)
    np.random.seed(const.SEED)
    indices  = np.random.permutation(const.NR_OF_IMAGES)
    train_idx, val_idx = indices[:splitIdx], indices[splitIdx:]
    x_train  = x[train_idx,:]
    y_train  = y[train_idx]
    x_val    = x[val_idx,:]
    y_val    = y[val_idx]
    return x_train, y_train, x_val, y_val


# Histogram equalization
def histEqualization(img):
    img_eq = exposure.equalize_hist(img)
    return img_eq  


def whitening(img):
    X_norm  = minMaxNorm(img)
    X_norm  = X_norm - X_norm.mean(axis=0)
    cov     = np.cov(X_norm, rowvar=True)
    U, S, V = np.linalg.svd(cov)    
    epsilon = 0.2
    X_ZCA = U.dot(np.diag(1.0/np.sqrt(S + epsilon))).dot(U.T).dot(X_norm)
    X_ZCA_rescaled = (X_ZCA - X_ZCA.min()) / (X_ZCA.max() - X_ZCA.min() + 0.00001)
    return X_ZCA_rescaled


def minMaxNorm(X):
    X = np.asarray(X)
    return X/255


#modified from https://stackoverflow.com/questions/22937589/how-to-add-noise-gaussian-salt-and-pepper-etc-to-image-in-python-with-opencv?rq=1
def addSaltAndPepper(img, probability):
    row, col = img.shape
    s_vs_p = probability
    amount = 0.05
    out = np.copy(img)
    # Salt mode
    num_salt = np.ceil(amount * img.size * s_vs_p)
    row_idx_s = np.random.randint(0, img.shape[0]-1, int(num_salt))
    col_idx_s = np.random.randint(0, img.shape[1]-1, int(num_salt))
    out[np.array(row_idx_s), np.array(col_idx_s)] = 1
    
    # Pepper mode
    num_pepper = np.ceil(amount* img.size * (1. - s_vs_p))
    row_idx_p = np.random.randint(0, img.shape[0]-1, int(num_pepper))
    col_idx_p = np.random.randint(0, img.shape[1]-1, int(num_pepper))
    out[np.array(row_idx_p), np.array(col_idx_p)] = 0
    return out


def laplace_of_gaussian(gray_img, sigma=1., kappa=0.75, pad=False):
    """
    Applies Laplacian of Gaussians to grayscale image.

    :param gray_img: image to apply LoG to
    :param sigma:    Gauss sigma of Gaussian applied to image, <= 0. for none
    :param kappa:    difference threshold as factor to mean of image values, <= 0 for none
    :param pad:      flag to pad output w/ zero border, keeping input image size
    """
    gray_img = np.asarray(gray_img)
    assert len(gray_img.shape) == 2
    img = cv2.GaussianBlur(gray_img, (0, 0), sigma) if 0. < sigma else gray_img
    img = cv2.Laplacian(img, cv2.CV_64F)
    #TODO if dtype >= float64 --> convert ;)
    rows, cols = img.shape[:2]
    # min/max of 3x3-neighbourhoods
    min_map = np.minimum.reduce(list(img[r:rows-2+r, c:cols-2+c]
                                     for r in range(3) for c in range(3)))
    max_map = np.maximum.reduce(list(img[r:rows-2+r, c:cols-2+c]
                                     for r in range(3) for c in range(3)))
    # bool matrix for image value positiv (w/out border pixels)
    pos_img = 0 < img[1:rows-1, 1:cols-1]
    # bool matrix for min < 0 and 0 < image pixel
    neg_min = min_map < 0
    neg_min[1 - pos_img] = 0
    # bool matrix for 0 < max and image pixel < 0
    pos_max = 0 < max_map
    pos_max[pos_img] = 0
    # sign change at pixel?
    zero_cross = neg_min + pos_max
    # values: max - min, scaled to 0--255; set to 0 for no sign change
    value_scale = 255. / max(1., img.max() - img.min())
    values = value_scale * (max_map - min_map)
    values[1 - zero_cross] = 0.
    # optional thresholding
    if 0. <= kappa:
        thresh = float(np.absolute(img).mean()) * kappa
        values[values < thresh] = 0.
    log_img = values.astype(np.uint8)
    if pad:
        log_img = np.pad(log_img, pad_width=1, mode='constant', constant_values=0)
        
    return log_img
