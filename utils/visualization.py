# -*- coding: utf-8 -*-
"""
Created on Sat Feb 15 13:57:10 2020

@author: Philipp
"""
import numpy as np
import matplotlib.pyplot as plt
import utils.constants as const

def plotImage(X):
    plt.imshow(X, cmap = 'gray')
    plt.show()
    plt.close()
    
def plotLearningCurve(hist, init_epoch):
    
    plt.plot(hist.history['accuracy'][init_epoch:init_epoch+const.NUMBER_OF_EPOCHS])
    plt.plot(hist.history['val_accuracy'][init_epoch:init_epoch+const.NUMBER_OF_EPOCHS])
    plt.title('Accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train'], loc='upper left')
    plt.show()
    '''
    plt.plot(hist.history['categorical_accuracy'][init_epoch:init_epoch+const.NUMBER_OF_EPOCHS])
    plt.plot(hist.history['val_categorical_accuracy'][init_epoch:init_epoch+const.NUMBER_OF_EPOCHS])
    plt.title('Categorical accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train'], loc='upper left')
    plt.show()
    '''
    plt.plot(hist.history['loss'][init_epoch:init_epoch+const.NUMBER_OF_EPOCHS])
    plt.plot(hist.history['val_loss'][init_epoch:init_epoch+const.NUMBER_OF_EPOCHS])
    plt.title('Loss')
    plt.xlabel('epoch')
    plt.xlabel('loss')
    plt.legend(['train'], loc='upper right')
    plt.show()
    '''
    plt.plot(hist.history['loss'][const.NUMBER_OF_EPOCHS-(int(const.NUMBER_OF_EPOCHS*0.7)):const.NUMBER_OF_EPOCHS])
    plt.plot(hist.history['val_loss'][const.NUMBER_OF_EPOCHS-(int(const.NUMBER_OF_EPOCHS*0.7)):NUMBER_OF_EPOCHS])
    plt.title('Loss of the last 70% of the epochs')
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.legend(['train'], loc='upper right')
    plt.show()
    '''

# modified from stackoverflow (https://stackoverflow.com/questions/39280813/visualization-of-convolutional-layer-in-keras-model)
def plot_conv_weights(model, layer_name):
    W = model.get_layer(name = layer_name).get_weights()[0]
    if len(W.shape) == 4:
        W = np.squeeze(W)
        if len(W.shape) == 3:
            W = W.reshape((W.shape[0], W.shape[1], W.shape[2])) 
        else:
            W = W.reshape((W.shape[0], W.shape[1], W.shape[2]*W.shape[3])) 
        fig, axs = plt.subplots(5, 5, figsize=(8, 8))
        fig.subplots_adjust(hspace = 0.5, wspace = 0.001)
        axs = axs.ravel()
        for i in range(25):
            axs[i].imshow(W[:, :, i], cmap='gray')
        plt.show()


def plot_large_conv_weights(model, layer_name):
    W = model.get_layer(name = layer_name).get_weights()[0]
    if len(W.shape) == 4:
        W = np.squeeze(W)
        if len(W.shape) == 3:
            W = W.reshape((W.shape[0], W.shape[1], W.shape[2])) 
        else:
            W = W.reshape((W.shape[0], W.shape[1])) 

        fig, axs = plt.subplots(3, 3, figsize=(12, 12))
        fig.subplots_adjust(hspace = 0.5, wspace = 0.001)
        axs = axs.ravel()
        for i in range(9):
            axs[i].imshow(W, cmap='gray')
        plt.show()
